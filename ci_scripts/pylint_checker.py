#!/usr/bin/env python3
"""
Module that runs pylint on all python scripts found in a directory tree..
"""

import os
import re
import sys

total = 0.0
count = 0


def check(module):
    """
    Apply pylint to the file specified if it is a *.py file
    """
    global total, count

    if module[-3:] == ".py":

        # print("CHECKING ", module)
        pout = os.popen('pylint %s' % module, 'r')
        for line in pout:
            if re.match("E....:.", line):
                # print(line)
                pass
            if "Your code has been rated at" in line:
                # print(line)
                negative_matches = re.findall("-\d+.\d\d", line)
                if len(negative_matches) > 0:
                    score = negative_matches[0]
                else:
                    score = re.findall("\d+.\d\d", line)[0]
                total += float(score)
                count += 1


if __name__ == "__main__":
    try:
        print(sys.argv)
        BASE_DIRECTORY = sys.argv[1]
    except IndexError:
        print("no directory specified, defaulting to current working directory")
        BASE_DIRECTORY = os.getcwd()

    print("looking for *.py scripts in subdirectories of ", BASE_DIRECTORY)
    for root, dirs, files in os.walk(BASE_DIRECTORY):
        for name in files:
            filepath = os.path.join(root, name)
            check(filepath)

    print("==" * 50)
    print("%d modules found" % count)

average_score_str = "%.02f" % (total / count)
print("AVERAGE SCORE = %s" % average_score_str)

# Get the dir this script is in
script_dir = os.path.dirname(os.path.realpath(__file__))

score_file = open(os.path.join(script_dir, "generated_files", "pylint_score.txt"), 'w')
score_file.write(average_score_str)
score_file.close()

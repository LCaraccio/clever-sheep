.. _downloads:

Downloads
#########

This page contains tar.gz files available for download. CleverSheep is only
supported on unix based systems.

To install:

1. Download the latest release.
2. Untar and change into the created directory.
3. Use setup.py to build then install the package.

For example:

.. code-block:: none

    tar xf CleverSheep-0.5.10.tar.gz
    cd CleverSheep-0.5.10
    python setup.py build
    sudo python setup.py install

============= ============ =============
Version       Release Date Download Link
============= ============ =============
0.7.6         2021/03/08   `CleverSheep-0.7.6.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.6.tar.gz>`_
0.7.5         2020/11/15   `CleverSheep-0.7.5.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.5.tar.gz>`_
0.7.4         2020/10/07   `CleverSheep-0.7.4.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.4.tar.gz>`_
0.7.3         2020/07/13   `CleverSheep-0.7.3.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.3.tar.gz>`_
0.7.2         2020/06/15   `CleverSheep-0.7.2.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.2.tar.gz>`_
0.7.1         2020/06/10   `CleverSheep-0.7.1.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.1.tar.gz>`_
0.7.0         2020/01/31   `CleverSheep-0.7.0.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.7.0.tar.gz>`_
0.6.5         2018/04/20   `CleverSheep-0.6.5.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.6.5.tar.gz>`_
0.6.4         2017/10/29   `CleverSheep-0.6.4.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.6.4.tar.gz>`_
0.6.3         2017/10/17   `CleverSheep-0.6.3.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.6.3.tar.gz>`_
0.6.2         2017/09/23   `CleverSheep-0.6.2.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.6.2.tar.gz>`_
0.6.1         2017/07/16   `CleverSheep-0.6.1.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.6.1.tar.gz>`_
0.6.0         2017/04/03   `CleverSheep-0.6.0.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.6.0.tar.gz>`_
0.5.10        2014/12/10   `CleverSheep-0.5.10.tar.gz <https://gitlab.com/LCaraccio/clever-sheep-releases/raw/master/CleverSheep-0.5.10.tar.gz>`_
============= ============ =============

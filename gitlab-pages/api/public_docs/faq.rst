.. _faq:

FAQ
###

You may want to read the :ref:`troubleshooting` page if you are having specific
issues with CleverSheep. You may also want to take a look at the :ref:`code_snippets`
page if you looking for how to do something specific.

.. contents::
    :local:
    :depth: 1

Is Python 3 supported?
======================

As of 0.7.0 yes but only 3.6+.

What is it?
===========
CleverSheep is a python package focused on high-level automated testing.
It is event driven in nature, namely events are made based on observable
behavior of the system under test (SUT) which can then be expected to
occur asynchronously in testing.

Should I use it?
================
Probably not.

The library has existed for a long time and when it was originally written it filled a gap. That gap has now been filled
with other libraries that are far better in terms of documentation, community and usability, the prime example being pytest.

If there is something the library does that others do not reach out and potentially this could be
spun out as a stand alone library or plugin for another test framework which is how https://lcaraccio.gitlab.io/tes-lib
came to be as a stand alone implementation of the event store functionality.

It is recommended any current users put in place plans to migrate away from CleverSheep.

What OS variants are supported?
===============================

It should work on any Unix based system. The tests are only run on CentOS.

Can I use the twisted or tornado event loop managers?
=====================================================

Yes see the :ref:`code_snippets` page for more details but there may be issues using later versions
of twisted.
.. CleverSheep documentation master file

Welcome to CleverSheep's documentation!
=======================================

CleverSheep is a Python package largely focused on high-level automated
testing. It provides a number of features including a test runner, support for
custom command line arguments and the ability to write asynchronous tests
through the test event store.

Some key features include:
  * A built in test runner with a wealth of command line options.
  * Support for user defined command line options.
  * Support for complex test selection logic.

If you want to raise an issue visit https://gitlab.com/LCaraccio/clever-sheep

If you are interested in only using the event store see https://lcaraccio.gitlab.io/tes-lib

.. warning:: It is not recommended to use CleverSheep for new projects, see the FAQ for more.

Release Notes
-------------

See the :ref:`release_notes` for the details of each release.

Installation
------------

You can simply install CleverSheep using pip:

.. code-block:: none

    pip install CleverSheep

Or if you prefer you can get the latest version from the :ref:`downloads` page.

FAQ
---

Got a question, see the :ref:`faq` page.

Documentation
=============

.. toctree::
   :name: mastertoc
   :maxdepth: 2

   public_docs/basics
   public_docs/clever_sheep_tests
   public_docs/command_line_options
   public_docs/clever_sheep_testing_model
   public_docs/library_options
   public_docs/poll_manager
   public_docs/test_event_store
   public_docs/mock_support
   public_docs/troubleshooting
   public_docs/code_snippets
   public_docs/glossary

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

CleverSheep contains some code that has been reused or simply copied.

  +----------------+-----------------------------------------------------+
  | Module         | Information                                         |
  +================+=====================================================+
  | ultraTB.py     | A neat module written by Nathan Gray.               |
  +----------------+-----------------------------------------------------+
  | Struct.py      | Inspired by an ASPN recipe by Brian McErlean.       |
  |                |                                                     |
  |                | It is possible that some snippits of his code have  |
  |                | made it into mine.                                  |
  +----------------+-----------------------------------------------------+

If you think this list is incomplete email `cleversheepframework@gmail.com` and
let us know.

A big thanks to the original author of CleverSheep Paul Ollis.
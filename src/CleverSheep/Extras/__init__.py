"""This package contains copies of third party modules/packages.

This is not intended to be used directly. It is used indirectly when the
'real' package is not available.

The modules in here are:

+---------------+---------+---------------------------------------------------+
| Module        | Version | Notes                                             |
+===============+=========+===================================================+
| ultraTBy      | 0.3     | Nathan Gray's module that provides coloured       |
|               |         | traceback output.                                 |
|               |         |                                                   |
|               |         | See: http://www.n8gray.org/files/ultraTB.py       |
+---------------+---------+---------------------------------------------------+

"""



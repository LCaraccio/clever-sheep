#!/usr/bin/env python
"""Utilities for data visualisation.

This package contains modules that support visualisation of data.
The modules include.

    `Msc`
        Support for drawing message sequence charts.
    `Tree`
        Support for drawing text tree diagrams.
"""


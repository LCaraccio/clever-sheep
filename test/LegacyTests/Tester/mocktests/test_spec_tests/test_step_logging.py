#!/usr/bin/env python
"""Step logging test suite."""

from CleverSheep.Test.Tester import *


class TestBaseA(Suite):

    def base_a_level_1(self):
        # > TestBaseA step level 1
        print("TestBaseA step level 1 is called")
        self.base_a_level_2()

    def base_a_level_2(self):
        # > TestBaseA step level 2
        print("TestBaseA step level 2 is called")
        self.base_a_level_3()

    def base_a_level_3(self):
        # > TestBaseA step level 3
        print("TestBaseA step level 3 is called")


class TestBaseB(TestBaseA):

    def base_b_level_1(self):
        # > TestBaseB step level 1
        print("TestBaseB step level 1 is called")
        self.base_b_level_2()

    def base_b_level_2(self):
        # > TestBaseB step level 2
        print("TestBaseB step level 2 is called")
        self.base_b_level_3()

    def base_b_level_3(self):
        # > TestBaseB step level 3
        print("TestBaseB step level 3 is called")
        self.base_a_level_1()


class TestSuite(TestBaseB):
    """Step logging test suite."""

    def suite_level_1(self):
        # > TestSuite step level 1
        print("TestSuite step level 1 is called")
        self.suite_level_2()

    def suite_level_2(self):
        # > TestSuite step level 2
        print("TestSuite step level 2 is called")
        self.suite_level_3()

    def suite_level_3(self):
        # > TestSuite step level 3
        print("TestSuite step level 3 is called")
        self.base_b_level_1()

    @test(testID="step-logging-01")
    def test_step_logging_01(self):
        """test_step_logging_01."""
        # > Call suite_level_1
        self.suite_level_1()
        # > Call base_b_level_1
        self.base_b_level_1()
        # > Call base_a_level_1
        self.base_a_level_1()


if __name__ == "__main__":
    runModule()


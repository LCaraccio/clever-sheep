#!/usr/bin/env python
"""Step level skipping test suite."""

from CleverSheep.Test.Tester import *


class TestSuite(Suite):
    """Step level skipping test suite."""

    def _step_2_down_call_2(self):
        # > Test step 2 down from top level
        assert True

    def step_2_down(self):
        self._step_2_down_call_2()

    def _step_3_down_call_3(self):
        # > Test step 3 down from top level
        assert True

    def _step_3_down_call_2(self):
        self._step_3_down_call_3()

    def step_3_down(self):
        self._step_3_down_call_2()

    def step_3_down_and_1_down(self):
        self._step_3_down_call_2()
        # > Test step 1 down from top level
        assert True

    @test(testID="skip-steps-2-down")
    def test_skip_steps_2_down(self):
        """skip-steps-2-down."""
        # > Top level step 1
        self.step_2_down()
        # > Top level step 2
        print("Done")

    @test(testID="skip-steps-3-down")
    def test_skip_steps_3_down(self):
        """skip-steps-3-down."""
        # > Top level step 1
        self.step_3_down()
        # > Top level step 2
        print("Done")

    @test(testID="skip-steps-3-then-1-down")
    def test_skip_steps_3_then_1_down(self):
        """skip-steps-3-then-1-down."""
        # > Top level step 1
        self.step_3_down_and_1_down()
        # > Top level step 2
        print("Done")

    @test(testID="no-top-level-steps")
    def test_no_top_level_steps(self):
        """test the numbering with no top level steps."""
        self.step_3_down_and_1_down()
        print("Done")


if __name__ == "__main__":
    runModule()


#!/usr/bin/env python
"""Step numbering test suite."""

from CleverSheep.Test.Tester import *


class AppleSuite(Suite):
    """Step numbering test suite."""

    @test(testID="step-numbering-01")
    def test_apples_1(self):
        """Test step numbering."""
        # > This is step 1
        self.method1()
        self.method2()

    def method1(self):
        # > This is step 1.1
        print("In method1")
        # > This is step 1.2
        print("In method1")

    def method2(self):
        # > This is step 1.3
        print("In method2")
        # > This is step 1.4
        print("In method2")


if __name__ == "__main__":
    runModule()


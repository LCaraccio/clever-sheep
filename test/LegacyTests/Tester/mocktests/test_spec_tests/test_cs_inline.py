#!/usr/bin/env python
"""cs_inline decorator test suite."""

from CleverSheep.Test.Tester import *


class TestSuite(Suite):
    """cs_inline decorator test suite."""

    @cs_inline
    def cs_inlined_method(self):
        # > Test step in cs_inlined_method
        print("In cs_inlined_method")

    def non_inlined_method(self):
        # > Test step in non_inlined_method
        print("In non_inlined_method")

    @test(testID="cs-inline-no-top-level-steps")
    def test_cs_inline_no_top_level_steps(self):
        """cs-inline-no-top-level-steps."""
        print("Before cs_inlined_method")
        self.cs_inlined_method()
        print("After cs_inlined_method")

    @test(testID="cs-inline-with-top-level-steps")
    def test_cs_inline_with_top_level_steps(self):
        """cs-inline-with-top-level-steps."""
        # > Top-level step before cs_inlined_method
        print("Before cs_inlined_method")
        self.cs_inlined_method()
        print("After cs_inlined_method")
        # > Top-level step after cs_inlined_method
        assert True

    def method_calling_cs_inlined_method(self):
        # > Sub step before cs_inlined_method
        print("Before cs_inlined_method")
        self.cs_inlined_method()
        print("After cs_inlined_method")
        # > Sub step after cs_inlined_method
        assert True

    @test(testID="cs-inline-from-intermediate-method")
    def test_cs_inline_from_intermediate_method(self):
        """cs-inline-from-intermediate-method."""
        # > Top-level step before method_calling_cs_inlined_method
        print("Before method_calling_cs_inlined_method")
        self.method_calling_cs_inlined_method()
        print("After method_calling_cs_inlined_method")
        # > Top-level step after method_calling_cs_inlined_method
        assert True

    def no_steps_method_calling_cs_inlined_method(self):
        print("Before cs_inlined_method")
        self.cs_inlined_method()
        print("After cs_inlined_method")
        assert True

    @test(testID="cs-inline-from-intermediate-method-with-no-steps")
    def test_cs_inline_from_intermediate_method_with_no_steps(self):
        """cs-inline-from-intermediate-method-with-no-step."""
        # > Top-level step before no_steps_method_calling_cs_inlined_method
        print("Before no_steps_method_calling_cs_inlined_method")
        self.no_steps_method_calling_cs_inlined_method()
        print("After no_steps_method_calling_cs_inlined_method")
        # > Top-level step after no_steps_method_calling_cs_inlined_method
        assert True

    @cs_inline
    def cs_inlined_method_with_no_steps(self):
        print("In cs_inlined_method_with_no_steps")

    @test(testID="cs-inline-with-no-steps")
    def test_cs_inline_with_no_steps(self):
        """cs-inline-with-no-steps."""
        # > Top-level step before cs_inlined_method_with_no_steps
        print("Before cs_inlined_method_with_no_steps")
        self.cs_inlined_method_with_no_steps()
        print("After cs_inlined_method_with_no_steps")
        # > Top-level step after cs_inlined_method_with_no_steps
        assert True

    @cs_inline
    def cs_inlined_method_calling_cs_inlined_method(self):
        # > Intermediate step before cs_inlined_method
        print("Before cs_inlined_method")
        self.cs_inlined_method()
        print("After cs_inlined_method")
        # > Intermediate step after cs_inlined_method
        assert True

    @test(testID="cs-inline-calling-cs-inline")
    def test_cs_inline_calling_cs_inline(self):
        """cs-inline-calling-cs-inline."""
        # > Top-level step before cs_inlined_method_calling_cs_inlined_method
        print("Before cs_inlined_method_calling_cs_inlined_method")
        self.cs_inlined_method_calling_cs_inlined_method()
        print("After cs_inlined_method_calling_cs_inlined_method")
        # > Top-level step after cs_inlined_method_calling_cs_inlined_method
        assert True

    @cs_inline
    def cs_inlined_method_calling_non_inlined_method(self):
        # > Intermediate step before non_inlined_method
        print("Before non_inlined_method")
        self.non_inlined_method()
        print("After non_inlined_method")
        # > Intermediate step after non_inlined_method
        assert True

    @test(tsetID="cs-inline-calling-non-inline")
    def test_cs_inline_calling_non_inline(self):
        """cs-inline-calling-non-inline."""
        # > Top-level step before cs_inlined_method_calling_non_inlined_method
        print("Before cs_inlined_method_calling_non_inlined_method")
        self.cs_inlined_method_calling_non_inlined_method()
        print("After cs_inlined_method_calling_non_inlined_method")
        # > Top-level step after cs_inlined_method_calling_non_inlined_method
        assert True

    @cs_inline
    def cs_inlined_method_no_steps_calling_cs_inlined_method(self):
        print("Before cs_inlined_method")
        self.cs_inlined_method()
        print("After cs_inlined_method")

    @test(testID="cs-inline-no-steps-calling-cs-inline")
    def test_cs_inline_no_steps_calling_cs_inline(self):
        """cs-inline-no-steps-calling-cs-inline."""
        # > Top-level step before cs_inlined_method_no_steps_calling_cs_inlined_method
        print("Before cs_inlined_method_no_steps_calling_cs_inlined_method")
        self.cs_inlined_method_no_steps_calling_cs_inlined_method()
        print("After cs_inlined_method_no_steps_calling_cs_inlined_method")
        # > Top-level step after cs_inlined_method_no_steps_calling_cs_inlined_method
        assert True

    @cs_inline
    def cs_inlined_method_no_steps_calling_non_inlined_method(self):
        print("Before non_inlined_method")
        self.non_inlined_method()
        print("After non_inlined_method")

    @test(tsetID="cs-inline-no-steps-calling-non-inline")
    def test_cs_inline_no_steps_calling_non_inline(self):
        """cs-inline-no-steps-calling-non-inline."""
        # > Top-level step before cs_inlined_method_no_steps_calling_non_inlined_method
        print("Before cs_inlined_method_no_steps_calling_non_inlined_method")
        self.cs_inlined_method_no_steps_calling_non_inlined_method()
        print("After cs_inlined_method_no_steps_calling_non_inlined_method")
        # > Top-level step after cs_inlined_method_no_steps_calling_non_inlined_method
        assert True


if __name__ == "__main__":
    runModule()

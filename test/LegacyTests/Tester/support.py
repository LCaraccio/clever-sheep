"""Support for a number of test scripts.

"""
from __future__ import print_function

import os
import re
import subprocess
import time

from CleverSheep.Test import Tester
from CleverSheep.Prog import Files
from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import Suites

control = Suites.control()

class Struct(object):
    pass

_data = Struct()


rAddress = re.compile(r' at 0x[0-9a-f]+>')
rPath = re.compile(
        r'File "(/.*)/cleversheep/CleverSheep/Test/Tester/Suites.py",'
        r' line 124, in __new__')
rBad1 = re.compile(
        r"Module '(.*)/test/LegacyTests/Tester/mocktests/test_broken.py'"
        r" does not have a docstring")
rDots = re.compile(r'\.{10,}')


def clean(text):
    text = rAddress.sub(" at IGNORED>", text)
    m = rBad1.search(text)
    if m:
        text = text.replace(m.group(1), "<-->")
    text = rDots.sub("..........", text)
    return text


def checkTerminalOutput(expected, actual):
    expected = DataMaker.literalText2Text(expected)
    Tester.failUnlessEqualStrings(expected, actual, lineWrapper=clean)


def checkFailureOutput(expected, actual):
    expected = makeFailOutputText(expected)
    Tester.failUnlessEqualStrings(expected, actual, lineWrapper=clean)


class TestRunner(Tester.Suite):
    """Base class for test the execute other tests.

    Tests are typically executed in or below the sub-directory calles
    'mocktests'.

    """
    def postCleanUp(self):
        if not Tester.currentTestHasFailed():
            self.cleanUp()

    def cleanUp(self):
        #Files.rmFile("subtest.log")
        #Files.rmFile("temp.log")
        pass

    def setUp(self):
        # Change to the mock tests directory.
        os.chdir(os.path.join(os.path.dirname(__file__), "mocktests"))
        self.cleanUp()

    def tearDown(self):
        if Tester.currentTestHasFailed():
            Tester.console.write("%s\n" % getSubtestFileInfo())
        self.postCleanUp()


def run_test(script, *testArgs, **kwargs):
    """Run a test script, with coverage enabled if requested

    :Parameters:
      script
        The script to be executed, which is assumed to be a test script.
      testArgs
        Arguments passed to the script's command line.
      kwargs
        Keyword args are use by this function.
    :Return:
        A structure containing the following attributes.

        exitCode
           The exit code of the test script.
        cmd
           The command string used to invoke the test.
        log
           The contents of the test script's log file.
        script
            The text of the test script.
        data
            The contents of the 'data.log' file created by the script. If the
            script does not write such a file then this is ``None``.
    """
    exitCode = kwargs.pop("exitCode", 0)
    maxTime = kwargs.pop("maxTime", 5.0)
    startDir = kwargs.pop("startDir", None)
    enableJournal = kwargs.pop("enableJournal", 1)
    cmd = "./%s %s" % (script, " ".join(testArgs))
    options = []
    if enableJournal:
        options.append("--enable-journal")
    Tester.setCommentary("Run: %s" % cmd)
    _data.cmd = cmd
    cmd = "python ./%s --columns=80 --log-file=temp.log %s %s >subtest.log" % (
            script, " ".join(testArgs), " ".join(options))
    cmd += " 2>&1 </dev/null"
    _data.cmd = cmd
    if startDir is not None:
        os.chdir(startDir)
    Files.rmFile("data.log")
    print("Exec dir is %r" % os.getcwd())
    print("Running %r" % cmd)
    proc = subprocess.Popen(cmd, shell=True)

    backStop = time.time() + maxTime
    while proc.poll() is None and time.time() < backStop:
        control.delay(0.01)
    _data.exitCode = proc.returncode
    print("Exit code is %r" % _data.exitCode)
    if _data.exitCode is None:
        print("Killing slow sub-test run %r" % cmd)
        proc.kill()
    proc.wait()
    _data.exitCode = proc.returncode

    try:
        _data.log = open("temp.log").read()
    except IOError:
        # Occurs if there was a loading error.
        _data.log = ""
    _data.terminal = open("subtest.log").read()
    _data.script = open(script).read()
    if exitCode is not None:
        if _data.exitCode != exitCode:
            Tester.fail(
                    "Script:             %s"
                  "\nIn directory:       %s"
                  "\nExpected exit code: %d"
                  "\nGot exit code:      %d"
                  "\n%s" % (
                      script, os.getcwd(), exitCode, _data.exitCode,
                      getSubtestFileInfo()))

    _data.data = None
    if os.path.exists("data.log"):
        with open("data.log") as f:
            _data.data = f.read()

    return _data


def getSubtestFileInfo():
    return ("Terminal output saved in:"
            "\n  %s"
            "\nScript test log saved in:"
            "\n  %s" % (os.path.join(os.getcwd(), "subtest.log"),
                        os.path.join(os.getcwd(), "temp.log")))


def details():
    d = []
    d.append("Command: %s" % _data.cmd)
    d.append("Run output:")
    for l in _data.terminal.splitlines():
        d.append("    %s" % l.rstrip())
    d.append("Log output:")
    for l in _data.log.splitlines():
        d.append("    %s" % l.rstrip())
    d.append("Test script:")
    for l in _data.script.splitlines():
        d.append("    %s" % l.rstrip())

    return "\n".join(d)


def makeAllTests(dirpath, **kwargs):
    path = os.path.join(dirpath, "all_tests.py")
    data = {"modDoc": '"""Testing the tester."""'}
    for n in ("modDoc",):
        data["modDoc"] = kwargs.pop("modDoc", data[n])
    text = kwargs.pop("text", '''
    | #!/usr/bin/env python
    | %(modDoc)s
    | import CheckEnv
    | from CleverSheep.Test.Tester import *
    |
    | if __name__ == "__main__":
    |     runTree()
    ''' % data)
    return DataMaker.makeFile(path, text, mode="u+x", **kwargs)


def makeCheckEnv(dirpath, **kwargs):
    parts = dirpath.split(os.sep)
    path = os.path.join(dirpath, "CheckEnv.py")
    upPath = os.sep.join([".."] * (len(parts) + 3))
    text = '''
    | #!/usr/bin/env python
    | import os
    | import sys
    |
    | projDir = os.path.abspath(os.path.join(os.path.dirname(__file__),
    |                           "%s"))
    | pypath = os.environ.get("PYTHONPATH", "").split()
    | pypath.append(projDir)
    | os.environ["PYTHONPATH"] = ":".join(pypath)
    |
    | if sys.path[0] != projDir:
    |     sys.path[0:0] = [projDir]
    ''' % upPath

    return DataMaker.makeFile(path, text, mode="u+x", **kwargs)


def makeTestDir(dirpath):
    makeAllTests(dirpath)
    makeCheckEnv(dirpath)


def makePyFile(path, **kwargs):
    """Python specific wrapper for DataMaker.makeFile.

    This is a convenience function that seeks to avoid the need to supply much
    of the boiler plate code.

    For example, to create a test script with a single test.
    :<py>:

        support.makePySingleTestFile('test_a.py', text='''
        | class A(Suite):
        |     'Suite A'
        |     @test
        |     def a(self):
        |         'Test A'
        |         self.data = []
        |         self.control.addCallback(self.cb)
        ''')

    The surrounding module and class suite code is automatically supplied.

    :Parameters:
      path
        The pathname for the test file.
      text
        The main text of the test script. This must form one or more complete
        test suites.

    """
    text = kwargs.pop("text")
    data = {"modDoc": '"""Testing the tester."""'}
    for n in ("modDoc",):
        data["modDoc"] = kwargs.pop("modDoc", data[n])
    header = kwargs.pop("header", '''
    | #!/usr/bin/env python
    | %(modDoc)s
    | from __future__ import print_function
    | import CheckEnv
    | from CleverSheep.Test.Tester import *
    ''' % data)
    tail = '''
    |
    | if __name__ == "__main__":
    |     runModule()
    |
    '''
    return DataMaker.makeFile(path, [header, text, tail], **kwargs)


def makeTestScript(path, **kwargs):
    """Make a complete test script from various components.

    """
    tests = kwargs.pop("tests")
    suiteDocArgs = kwargs.pop("suiteDocs", [""] * len(tests))
    script = []
    for i, text in enumerate(tests):
        name = chr(ord('A') + i)
        doc = suiteDocArgs[i] or "'''Suite %s'''" % name
        script.append('''
        | class Suite%s(Suite):
        |     %s
        |     @test
        ''' % (name, doc))
        script.append(indent(tests[i], "    "))
    text = "\n".join(script)

    return makePyFile(path, text=text, mode="u+x", **kwargs)


def makePySingleTestFile(path, **kwargs):
    """Python specific wrapper for single test using DataMaker.makeFile.

    This is a convenience function that seeks to avoid the need to supply much
    of the boiler plate code. It can be used when you need to create a test
    script with just a single suite.

    For example, to create a test script with a single test.
    :<py>:

        support.makePySingleTestFile('test_a.py', text='''
        | def a(self):
        |    "Test A"
        ''')

    The surrounding module andclass suite code is automatically supplied.

    :Parameters:
      path
        The pathname for the test file.
      kwargs
        Additional keyword arguments.

    :Keywords:
      text
        The main text of the test.
      tests
        A sequence containing the text for mutiple tests. This is only used if
        ``text`` is not supplied.

    """
    suiteNames = [chr(ord('A') + i) for i in range(26)]
    text = kwargs.pop("text", None)
    if text is None:
        tests = kwargs.pop("tests")
    else:
        tests = [text]

    parts = []
    for n, text in zip(suiteNames, tests):
        key = "suite%s" % n
        if key in kwargs:
            parts.append(kwargs.pop(key))
        else:
            doc = kwargs.pop("suite%sDoc"% n, '"""Suite %s"""' % n)
            parts.append('''
            | class Suite%s(Suite):
            |     %s
            |     @test
            ''' % (n, doc))
        parts.append(indent(text, "    "))

    text = "\n".join(parts)

    return makePyFile(path, text=text, mode="u+x", **kwargs)


def makeFailOutputText(text):
    header = '''
    | Testing the tester.
    |   Suite A
    |     1   : Test A............................................               FAIL
    |           Test failed
    '''
    tail = '''
    |
    |
    | Summary of the failures
    | Testing the tester..........................................         CHILD_FAIL
    |   Suite A...................................................         CHILD_FAIL
    |     1   : Test A............................................               FAIL
    '''
    return DataMaker.literalText2Text(
            [header, indent(text, "          "), tail])


def indent(text, ind):
    rep = "|%s" % ind
    return "\n".join(line.replace("|", rep, 1) for line in text.splitlines())

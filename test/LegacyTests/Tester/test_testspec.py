#!/usr/bin/env python
"""Test Spec step output
"""

import os

import CheckEnv

from CleverSheep.Test import DataMaker
from CleverSheep.Test.Tester import *

import support


class Common(support.TestRunner):
    def setUp(self):
        super(Common, self).setUp()
        os.chdir("test_spec_tests")
        # Change to the mock tests directory.
        self.d = {"base": os.path.abspath(os.path.dirname(__file__))}
        self.info = {
            "base": os.path.dirname(os.path.abspath(__file__)),
            "cwd": os.getcwd(),
        }
        self.cleanUp()


class Test_testspec(Common):
    """Test the test spec produces the expected output
    """

    @test
    def test_spec_produces_expected_log(self):
        """Test that the expected log is produced by the test spec
        """
        support.run_test("test_step_logging.py")

        expected_lines = DataMaker.literalText2Text('''
        | INFO    Suite: Step logging test suite.
        | INFO    Suite:   Step logging test suite.
        | INFO    Test:      1   : [step-logging-01] test_step_logging_01.
        | INFO                     #> 1. Call suite_level_1
        | INFO                     #> 1.1. TestSuite step level 1
        | DEBUG                    TestSuite step level 1 is called
        | INFO                     #> 1.1.1. TestSuite step level 2
        | DEBUG                    TestSuite step level 2 is called
        | INFO                     #> 1.1.1.1. TestSuite step level 3
        | DEBUG                    TestSuite step level 3 is called
        | INFO                     #> 1.1.1.1.1. TestBaseB step level 1
        | DEBUG                    TestBaseB step level 1 is called
        | INFO                     #> 1.1.1.1.1.1. TestBaseB step level 2
        | DEBUG                    TestBaseB step level 2 is called
        | INFO                     #> 1.1.1.1.1.1.1. TestBaseB step level 3
        | DEBUG                    TestBaseB step level 3 is called
        | INFO                     #> 1.1.1.1.1.1.1.1. TestBaseA step level 1
        | DEBUG                    TestBaseA step level 1 is called
        | INFO                     #> 1.1.1.1.1.1.1.1.1. TestBaseA step level 2
        | DEBUG                    TestBaseA step level 2 is called
        | INFO                     #> 1.1.1.1.1.1.1.1.1.1. TestBaseA step level 3
        | DEBUG                    TestBaseA step level 3 is called
        | INFO                     #> 2. Call base_b_level_1
        | INFO                     #> 2.1. TestBaseB step level 1
        | DEBUG                    TestBaseB step level 1 is called
        | INFO                     #> 2.1.1. TestBaseB step level 2
        | DEBUG                    TestBaseB step level 2 is called
        | INFO                     #> 2.1.1.1. TestBaseB step level 3
        | DEBUG                    TestBaseB step level 3 is called
        | INFO                     #> 2.1.1.1.1. TestBaseA step level 1
        | DEBUG                    TestBaseA step level 1 is called
        | INFO                     #> 2.1.1.1.1.1. TestBaseA step level 2
        | DEBUG                    TestBaseA step level 2 is called
        | INFO                     #> 2.1.1.1.1.1.1. TestBaseA step level 3
        | DEBUG                    TestBaseA step level 3 is called
        | INFO                     #> 3. Call base_a_level_1
        | INFO                     #> 3.1. TestBaseA step level 1
        | DEBUG                    TestBaseA step level 1 is called
        | INFO                     #> 3.1.1. TestBaseA step level 2
        | DEBUG                    TestBaseA step level 2 is called
        | INFO                     #> 3.1.1.1. TestBaseA step level 3
        | DEBUG                    TestBaseA step level 3 is called
        | INFO    Test:            PASS
        | INFO
        | INFO    Summary of the entire run
        | INFO    Suite: Step logging test suite..............................           PART_RUN
        | INFO    Suite:   Step logging test suite............................               PASS
        | INFO    Test:      1   : [step-logging-01] test_step_logging_01.....               PASS
        ''')

        actual_lines = open("temp.log").read()
        sanitized_actual_lines = []

        first_line = True
        for line in actual_lines.splitlines():
            # Ignore the first line which contains a date
            if first_line:
                first_line = False
                continue

            # Strip the timestamps off the actual lines
            sanitized_line = line[13:].strip()
            sanitized_actual_lines.append(sanitized_line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def test_spec_produces_expected_details(self):
        """Test that the expected details output is produced by the test spec
        """
        data = support.run_test("test_step_logging.py", "--details")

        support.checkTerminalOutput('''
        | Step logging test suite.
        | ========================
        | 
        | 
        |   Step logging test suite.
        |   ========================
        | 
        |     1   : [step-logging-01] test_step_logging_01.
        |  
        |  
        |                 1. Call suite_level_1
        | 
        |                    1.1. TestSuite step level 1
        |
        |                      1.1.1. TestSuite step level 2
        |
        |                        1.1.1.1. TestSuite step level 3
        |
        |                          1.1.1.1.1. TestBaseB step level 1
        |
        |                            1.1.1.1.1.1. TestBaseB step level 2
        |
        |                              1.1.1.1.1.1.1. TestBaseB step level 3
        |
        |                                1.1.1.1.1.1.1.1. TestBaseA step level 1
        |
        |                                  1.1.1.1.1.1.1.1.1. TestBaseA step level 2
        |
        |                                    1.1.1.1.1.1.1.1.1.1. TestBaseA step level 3
        | 
        |                 2. Call base_b_level_1
        | 
        |                    2.1. TestBaseB step level 1
        |
        |                      2.1.1. TestBaseB step level 2
        |
        |                        2.1.1.1. TestBaseB step level 3
        |
        |                          2.1.1.1.1. TestBaseA step level 1
        |
        |                            2.1.1.1.1.1. TestBaseA step level 2
        |
        |                              2.1.1.1.1.1.1. TestBaseA step level 3
        | 
        |                 3. Call base_a_level_1
        | 
        |                    3.1. TestBaseA step level 1
        |
        |                      3.1.1. TestBaseA step level 2
        |
        |                        3.1.1.1. TestBaseA step level 3
        | 
        |           Path:     ./test_step_logging.py
        |           Class:    TestSuite
        |           Function: test_step_logging_01
        | 
        | 
        ''', data.terminal)

    @test
    def test_spec_produces_expected_details_and_log_matches(self):
        """Test that the expected details output is produced by the test spec and that the numbers in the log match
        """
        # Run the details and check the output
        test_file = "test_numbering.py"
        data = support.run_test(test_file, "--details")

        support.checkTerminalOutput('''
        | Step numbering test suite.
        | ==========================
        | 
        | 
        |   Step numbering test suite.
        |   ==========================
        | 
        |     1   : [step-numbering-01] Test step numbering.
        |  
        |  
        |                 1. This is step 1
        | 
        |                    1.1. This is step 1.1
        |
        |                    1.2. This is step 1.2
        | 
        |                    1.3. This is step 1.3
        | 
        |                    1.4. This is step 1.4
        | 
        |           Path:     ./test_numbering.py
        |           Class:    AppleSuite
        |           Function: test_apples_1
        | 
        | 
        ''', data.terminal)

        # Now run the test and check the log
        support.run_test(test_file)

        expected_lines = DataMaker.literalText2Text('''
        | INFO    Suite: Step numbering test suite.
        | INFO    Suite:   Step numbering test suite.
        | INFO    Test:      1   : [step-numbering-01] Test step numbering.
        | INFO                     #> 1. This is step 1
        | INFO                     #> 1.1. This is step 1.1
        | DEBUG                    In method1
        | INFO                     #> 1.2. This is step 1.2
        | DEBUG                    In method1
        | INFO                     #> 1.3. This is step 1.3
        | DEBUG                    In method2
        | INFO                     #> 1.4. This is step 1.4
        | DEBUG                    In method2
        | INFO    Test:            PASS
        | INFO    
        | INFO    Summary of the entire run
        | INFO    Suite: Step numbering test suite............................               PASS
        | INFO    Suite:   Step numbering test suite..........................               PASS
        | INFO    Test:      1   : [step-numbering-01] Test step numbering....               PASS
        ''')

        actual_lines = open("temp.log").read()
        sanitized_actual_lines = []

        first_line = True
        for line in actual_lines.splitlines():
            # Ignore the first line which contains a date
            if first_line:
                first_line = False
                continue

            # Strip the timestamps off the actual lines
            sanitized_line = line[13:].strip()
            sanitized_actual_lines.append(sanitized_line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def test_spec_produces_expected_details_sub_steps(self):
        """Test that the expected details output is produced by the test spec when running a suite that has sub steps
        within functions who's parents do not contain steps
        """
        # Run the details and check the output
        test_file = "test_skip_step_levels.py"
        data = support.run_test(test_file, "--details")

        support.checkTerminalOutput('''
            | Step level skipping test suite.
            | ===============================
            |
            |
            |   Step level skipping test suite.
            |   ===============================
            |
            |     1   : [skip-steps-2-down] skip-steps-2-down.
            |
            |
            |                 1. Top level step 1
            |
            |                      1.1.1. Test step 2 down from top level
            |
            |                 2. Top level step 2
            |
            |           Path:     ./test_skip_step_levels.py
            |           Class:    TestSuite
            |           Function: test_skip_steps_2_down
            |
            |     2   : [skip-steps-3-down] skip-steps-3-down.
            |
            |
            |                 1. Top level step 1
            |
            |                        1.1.1.1. Test step 3 down from top level
            |
            |                 2. Top level step 2
            |
            |           Path:     ./test_skip_step_levels.py
            |           Class:    TestSuite
            |           Function: test_skip_steps_3_down
            |
            |     3   : [skip-steps-3-then-1-down] skip-
            |           steps-3-then-1-down.
            |
            |
            |                 1. Top level step 1
            |
            |                        1.1.1.1. Test step 3 down from top level
            |
            |                    1.2. Test step 1 down from top level
            |
            |                 2. Top level step 2
            |
            |           Path:     ./test_skip_step_levels.py
            |           Class:    TestSuite
            |           Function: test_skip_steps_3_then_1_down
            |
            |     4   : [no-top-level-steps] test the numbering with no
            |           top level steps.
            | 
            | 
            |                        1.1.1.1. Test step 3 down from top level
            | 
            |                    1.2. Test step 1 down from top level
            | 
            |           Path:     ./test_skip_step_levels.py
            |           Class:    TestSuite
            |           Function: test_no_top_level_steps
            |
            |
            ''', data.terminal)

        # Now run the test and check the log
        support.run_test(test_file)

        expected_lines = DataMaker.literalText2Text('''
            | INFO    Suite: Step level skipping test suite.
            | INFO    Suite:   Step level skipping test suite.
            | INFO    Test:      1   : [skip-steps-2-down] skip-steps-2-down.
            | INFO                     #> 1. Top level step 1
            | INFO                     #> 1.1. Test step 2 down from top level
            | INFO                     #> 2. Top level step 2
            | DEBUG                    Done
            | INFO    Test:            PASS
            | INFO    Test:      2   : [skip-steps-3-down] skip-steps-3-down.
            | INFO                     #> 1. Top level step 1
            | INFO                     #> 1.1. Test step 3 down from top level
            | INFO                     #> 2. Top level step 2
            | DEBUG                    Done
            | INFO    Test:            PASS
            | INFO    Test:      3   : [skip-steps-3-then-1-down] skip-
            | steps-3-then-1-down.
            | INFO                     #> 1. Top level step 1
            | INFO                     #> 1.1. Test step 3 down from top level
            | INFO                     #> 1.2. Test step 1 down from top level
            | INFO                     #> 2. Top level step 2
            | DEBUG                    Done
            | INFO    Test:            PASS
            | INFO    Test:      4   : [no-top-level-steps] test the numbering
            | with no top level steps.
            | INFO                     #> 1. Test step 3 down from top level
            | INFO                     #> 2. Test step 1 down from top level
            | DEBUG                    Done
            | INFO    Test:            PASS
            | INFO    
            | INFO    Summary of the entire run
            | INFO    Suite: Step level skipping test suite.......................               PASS
            | INFO    Suite:   Step level skipping test suite.....................               PASS
            | INFO    Test:      1   : [skip-steps-2-down] skip-steps-2-down......               PASS
            | INFO    Test:      2   : [skip-steps-3-down] skip-steps-3-down......               PASS
            | INFO    Test:      3   : [skip-steps-3-then-1-down] skip-
            | steps-3-then-1-down........................               PASS
            | INFO    Test:      4   : [no-top-level-steps] test the numbering
            | with no top level steps....................               PASS
            ''')

        actual_lines = open("temp.log").read()
        sanitized_actual_lines = []

        first_line = True
        for line in actual_lines.splitlines():
            # Ignore the first line which contains a date
            if first_line:
                first_line = False
                continue

            # Strip the timestamps off the actual lines
            sanitized_line = line[13:].strip()
            sanitized_actual_lines.append(sanitized_line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))

    @test
    def test_cs_inline(self):
        """Test inline calls produce the expected details and log output
        """
        # Run the details and check the output
        test_file = "test_cs_inline.py"
        data = support.run_test(test_file, "--details")

        support.checkTerminalOutput('''
            | cs_inline decorator test suite.
            | ===============================
            | 
            | 
            |   cs_inline decorator test suite.
            |   ===============================
            | 
            |     1   : [cs-inline-no-top-level-steps] cs-inline-no-top-
            |           level-steps.
            | 
            | 
            |                 1. Test step in cs_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_no_top_level_steps
            | 
            |     2   : [cs-inline-with-top-level-steps] cs-inline-with-
            |           top-level-steps.
            | 
            | 
            |                 1. Top-level step before cs_inlined_method
            | 
            |                 2. Test step in cs_inlined_method
            | 
            |                 3. Top-level step after cs_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_with_top_level_steps
            | 
            |     3   : [cs-inline-from-intermediate-method] cs-inline-
            |           from-intermediate-method.
            | 
            | 
            |                 1. Top-level step before method_calling_cs_inlined_method
            | 
            |                    1.1. Sub step before cs_inlined_method
            | 
            |                    1.2. Test step in cs_inlined_method
            | 
            |                    1.3. Sub step after cs_inlined_method
            | 
            |                 2. Top-level step after method_calling_cs_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_from_intermediate_method
            | 
            |     4   : [cs-inline-from-intermediate-method-with-no-steps]
            |           cs-inline-from-intermediate-method-with-no-step.
            | 
            | 
            |                 1. Top-level step before no_steps_method_calling_cs_inlined_method
            | 
            |                    1.1. Test step in cs_inlined_method
            | 
            |                 2. Top-level step after no_steps_method_calling_cs_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_from_intermediate_method_with_no_steps
            | 
            |     5   : [cs-inline-with-no-steps] cs-inline-with-no-steps.
            | 
            | 
            |                 1. Top-level step before cs_inlined_method_with_no_steps
            | 
            |                 2. Top-level step after cs_inlined_method_with_no_steps
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_with_no_steps
            | 
            |     6   : [cs-inline-calling-cs-inline] cs-inline-calling-
            |           cs-inline.
            | 
            | 
            |                 1. Top-level step before cs_inlined_method_calling_cs_inlined_method
            | 
            |                 2. Intermediate step before cs_inlined_method
            | 
            |                 3. Test step in cs_inlined_method
            | 
            |                 4. Intermediate step after cs_inlined_method
            | 
            |                 5. Top-level step after cs_inlined_method_calling_cs_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_calling_cs_inline
            | 
            |     7   : cs-inline-calling-non-inline.
            | 
            | 
            |                 1. Top-level step before cs_inlined_method_calling_non_inlined_method
            | 
            |                 2. Intermediate step before non_inlined_method
            | 
            |                    2.1. Test step in non_inlined_method
            | 
            |                 3. Intermediate step after non_inlined_method
            | 
            |                 4. Top-level step after cs_inlined_method_calling_non_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_calling_non_inline
            | 
            |     8   : [cs-inline-no-steps-calling-cs-inline] cs-inline-
            |           no-steps-calling-cs-inline.
            | 
            | 
            |                 1. Top-level step before cs_inlined_method_no_steps_calling_cs_inlined_method
            | 
            |                 2. Test step in cs_inlined_method
            | 
            |                 3. Top-level step after cs_inlined_method_no_steps_calling_cs_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_no_steps_calling_cs_inline
            | 
            |     9   : cs-inline-no-steps-calling-non-inline.
            | 
            | 
            |                 1. Top-level step before cs_inlined_method_no_steps_calling_non_inlined_method
            | 
            |                    1.1. Test step in non_inlined_method
            | 
            |                 2. Top-level step after cs_inlined_method_no_steps_calling_non_inlined_method
            | 
            |           Path:     ./test_cs_inline.py
            |           Class:    TestSuite
            |           Function: test_cs_inline_no_steps_calling_non_inline
            | 
            |
            ''', data.terminal)

        # Now run the test and check the log
        support.run_test(test_file)

        expected_lines = DataMaker.literalText2Text('''
            | INFO    Suite: cs_inline decorator test suite.
            | INFO    Suite:   cs_inline decorator test suite.
            | INFO    Test:      1   : [cs-inline-no-top-level-steps] cs-inline-
            | no-top-level-steps.
            | DEBUG                    Before cs_inlined_method
            | INFO                     #> 1. Test step in cs_inlined_method
            | DEBUG                    In cs_inlined_method
            | DEBUG                    After cs_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      2   : [cs-inline-with-top-level-steps] cs-inline-
            | with-top-level-steps.
            | INFO                     #> 1. Top-level step before cs_inlined_method
            | DEBUG                    Before cs_inlined_method
            | INFO                     #> 2. Test step in cs_inlined_method
            | DEBUG                    In cs_inlined_method
            | DEBUG                    After cs_inlined_method
            | INFO                     #> 3. Top-level step after cs_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      3   : [cs-inline-from-intermediate-method] cs-
            | inline-from-intermediate-method.
            | INFO                     #> 1. Top-level step before method_calling_cs_inlined_method
            | DEBUG                    Before method_calling_cs_inlined_method
            | INFO                     #> 1.1. Sub step before cs_inlined_method
            | DEBUG                    Before cs_inlined_method
            | INFO                     #> 1.2. Test step in cs_inlined_method
            | DEBUG                    In cs_inlined_method
            | DEBUG                    After cs_inlined_method
            | INFO                     #> 1.3. Sub step after cs_inlined_method
            | DEBUG                    After method_calling_cs_inlined_method
            | INFO                     #> 2. Top-level step after method_calling_cs_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      4   : [cs-inline-from-intermediate-method-with-
            | no-steps] cs-inline-from-intermediate-
            | method-with-no-step.
            | INFO                     #> 1. Top-level step before no_steps_method_calling_cs_inlined_method
            | DEBUG                    Before no_steps_method_calling_cs_inlined_method
            | DEBUG                    Before cs_inlined_method
            | INFO                     #> 2. Test step in cs_inlined_method
            | DEBUG                    In cs_inlined_method
            | DEBUG                    After cs_inlined_method
            | DEBUG                    After no_steps_method_calling_cs_inlined_method
            | INFO                     #> 3. Top-level step after no_steps_method_calling_cs_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      5   : [cs-inline-with-no-steps] cs-inline-with-
            | no-steps.
            | INFO                     #> 1. Top-level step before cs_inlined_method_with_no_steps
            | DEBUG                    Before cs_inlined_method_with_no_steps
            | DEBUG                    In cs_inlined_method_with_no_steps
            | DEBUG                    After cs_inlined_method_with_no_steps
            | INFO                     #> 2. Top-level step after cs_inlined_method_with_no_steps
            | INFO    Test:            PASS
            | INFO    Test:      6   : [cs-inline-calling-cs-inline] cs-inline-
            | calling-cs-inline.
            | INFO                     #> 1. Top-level step before cs_inlined_method_calling_cs_inlined_method
            | DEBUG                    Before cs_inlined_method_calling_cs_inlined_method
            | INFO                     #> 2. Intermediate step before cs_inlined_method
            | DEBUG                    Before cs_inlined_method
            | INFO                     #> 3. Test step in cs_inlined_method
            | DEBUG                    In cs_inlined_method
            | DEBUG                    After cs_inlined_method
            | INFO                     #> 4. Intermediate step after cs_inlined_method
            | DEBUG                    After cs_inlined_method_calling_cs_inlined_method
            | INFO                     #> 5. Top-level step after cs_inlined_method_calling_cs_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      7   : cs-inline-calling-non-inline.
            | INFO                     #> 1. Top-level step before cs_inlined_method_calling_non_inlined_method
            | DEBUG                    Before cs_inlined_method_calling_non_inlined_method
            | INFO                     #> 2. Intermediate step before non_inlined_method
            | DEBUG                    Before non_inlined_method
            | INFO                     #> 2.1. Test step in non_inlined_method
            | DEBUG                    In non_inlined_method
            | DEBUG                    After non_inlined_method
            | INFO                     #> 3. Intermediate step after non_inlined_method
            | DEBUG                    After cs_inlined_method_calling_non_inlined_method
            | INFO                     #> 4. Top-level step after cs_inlined_method_calling_non_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      8   : [cs-inline-no-steps-calling-cs-inline] cs-
            | inline-no-steps-calling-cs-inline.
            | INFO                     #> 1. Top-level step before cs_inlined_method_no_steps_calling_cs_inlined_method
            | DEBUG                    Before cs_inlined_method_no_steps_calling_cs_inlined_method
            | DEBUG                    Before cs_inlined_method
            | INFO                     #> 2. Test step in cs_inlined_method
            | DEBUG                    In cs_inlined_method
            | DEBUG                    After cs_inlined_method
            | DEBUG                    After cs_inlined_method_no_steps_calling_cs_inlined_method
            | INFO                     #> 3. Top-level step after cs_inlined_method_no_steps_calling_cs_inlined_method
            | INFO    Test:            PASS
            | INFO    Test:      9   : cs-inline-no-steps-calling-non-inline.
            | INFO                     #> 1. Top-level step before cs_inlined_method_no_steps_calling_non_inlined_method
            | DEBUG                    Before cs_inlined_method_no_steps_calling_non_inlined_method
            | DEBUG                    Before non_inlined_method
            | INFO                     #> 1.1. Test step in non_inlined_method
            | DEBUG                    In non_inlined_method
            | DEBUG                    After non_inlined_method
            | DEBUG                    After cs_inlined_method_no_steps_calling_non_inlined_method
            | INFO                     #> 2. Top-level step after cs_inlined_method_no_steps_calling_non_inlined_method
            | INFO    Test:            PASS
            | INFO    
            | INFO    Summary of the entire run
            | INFO    Suite: cs_inline decorator test suite.......................               PASS
            | INFO    Suite:   cs_inline decorator test suite.....................               PASS
            | INFO    Test:      1   : [cs-inline-no-top-level-steps] cs-inline-
            | no-top-level-steps.........................               PASS
            | INFO    Test:      2   : [cs-inline-with-top-level-steps] cs-inline-
            | with-top-level-steps.......................               PASS
            | INFO    Test:      3   : [cs-inline-from-intermediate-method] cs-
            | inline-from-intermediate-method............               PASS
            | INFO    Test:      4   : [cs-inline-from-intermediate-method-with-
            | no-steps] cs-inline-from-intermediate-
            | method-with-no-step........................               PASS
            | INFO    Test:      5   : [cs-inline-with-no-steps] cs-inline-with-
            | no-steps...................................               PASS
            | INFO    Test:      6   : [cs-inline-calling-cs-inline] cs-inline-
            | calling-cs-inline..........................               PASS
            | INFO    Test:      7   : cs-inline-calling-non-inline...............               PASS
            | INFO    Test:      8   : [cs-inline-no-steps-calling-cs-inline] cs-
            | inline-no-steps-calling-cs-inline..........               PASS
            | INFO    Test:      9   : cs-inline-no-steps-calling-non-inline......               PASS
            ''')

        actual_lines = open("temp.log").read()
        sanitized_actual_lines = []

        first_line = True
        for line in actual_lines.splitlines():
            # Ignore the first line which contains a date
            if first_line:
                first_line = False
                continue

            # Strip the timestamps off the actual lines
            sanitized_line = line[13:].strip()
            sanitized_actual_lines.append(sanitized_line)

        failUnlessEqualStrings(expected_lines, "\n".join(sanitized_actual_lines))


if __name__ == "__main__":
    runModule()

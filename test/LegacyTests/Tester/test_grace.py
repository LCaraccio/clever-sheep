#!/usr/bin/env python
"""Test for graceful handling of user errors.

"""
from __future__ import print_function

import CheckEnv

import sys

from CleverSheep.Test import Tester
from CleverSheep.Test.Tester import *

import support


class TestLoading(support.TestRunner):
    """Graceful behaviour of test loading issues.

    """
    @test(testID="missing-docstring-01", issue=83)
    def missing_suite_docstring(self):
        """A missing suite docstring should provoke a friendly and useful error
        message.

        """
        support.makePySingleTestFile("test_broken.py", suiteADoc="", text='''
        | def a(self):
        |    """Test A"""
        |    def err():
        |        raise TypeError("CoderError")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Test loading error
        | Suite 'SuiteA' in file ./test_broken.py does not have a docstring
        ''', data.terminal)

    @test(testID="missing-docstring-02")
    def missing_test_docstring(self):
        """A missing test docstring should provoke a friendly and useful error
        message.

        """
        support.makePySingleTestFile("test_broken.py", text='''
        | def a(self):
        |    def err():
        |        raise TypeError("CoderError")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Test loading error
        | Test 'a' in file ./test_broken.py does not have a docstring
        ''', data.terminal)

    @test(testID="missing-docstring-03")
    def missing_module_docstring(self):
        """A missing module docstring should provoke a friendly and useful
        error message.

        """
        support.makePySingleTestFile("test_broken.py", modDoc="", text='''
        | def a(self):
        |    """Test A"""
        |    def err():
        |        raise TypeError("CoderError")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkTerminalOutput('''
        | Test loading error
        | Module '<-->/test/LegacyTests/Tester/mocktests/test_broken.py' does not have a docstring
        ''', data.terminal)


class TestRuntime(support.TestRunner):
    """Graceful behaviour of unpleasant run-time failures.

    """
    @test("bug", testID="issue-81", issue=81)
    def bad_error_message_function(self):
        """A bad error message function should not give a cryptic failure.

        This was added for issue 81. LC: I don't know what issue 81 is.

        Note: This test is marked as bug, Python 2.7, 3.6 and 3.7 all give slightly different output. It's just not
        worth the time to make it handle all the cases
        """
        support.makePySingleTestFile("test_broken.py", text='''
        | def a(self):
        |    """Test A"""
        |    def err():
        |        raise TypeError("CoderError")
        |
        |    fail("Oops", makeMessage=err)
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        if sys.version[0:3] < '3':
            support.checkFailureOutput('''
            | !! Cannot correctly describe failure !!
            | The function <function err at IGNORED> raised the exception
            |     TypeError('CoderError',)
            | ./test_broken.py: a
            |    11  :        def err():
            |    12  :            raise TypeError("CoderError")
            |    13  :
            | ==>14  :        fail("Oops", makeMessage=err)
            |    15  :
            |    16  : if __name__ == "__main__":
            |    17  :     runModule()
            ''', data.terminal)
        else:
            support.checkFailureOutput('''
            | !! Cannot correctly describe failure !!
            | The function <function SuiteA.a.<locals>.err at IGNORED> raised the exception
            |     TypeError('CoderError',)
            | ./test_broken.py: a
            |    11  :        def err():
            |    12  :            raise TypeError("CoderError")
            |    13  :
            | ==>14  :        fail("Oops", makeMessage=err)
            |    15  :
            |    16  : if __name__ == "__main__":
            |    17  :     runModule()
            ''', data.terminal)

    @test(testID="random-exception")
    def non_cs_exception(self):
        """Non-cleversheep exceptions should result in an informative failure.
        """
        support.makePySingleTestFile("test_broken.py", text='''
        | def a(self):
        |    """Test A"""
        |    raise OSError("System Error")
        ''')
        data = support.run_test("test_broken.py", exitCode=1)

        support.checkFailureOutput('''
        | Unhandled exception occurred:
        |   OSError:System Error
        | ./test_broken.py: a
        |    8   :     @test
        |    9   :     def a(self):
        |    10  :        """Test A"""
        | ==>11  :        raise OSError("System Error")
        |    12  :
        |    13  : if __name__ == "__main__":
        |    14  :     runModule()
        ''', data.terminal)


if __name__ == "__main__":
    runModule()

#!/usr/bin/env python
"""CleverSheep.Prog.Process unit tests"""

import CheckEnv

from CleverSheep.Sys import Platform
if Platform.platformType == "windows":
    from CleverSheep.Test.Tester import Collection
    raise Collection.Unsupported

import os
import getpass
import subprocess
import time

from CleverSheep.Test.Tester import *
from CleverSheep.Test.Tester import Suites

# The module under test.
from CleverSheep.Prog import Process

control = Suites.control()


class Options(object):
    def __init__(self, **kwargs):
        self.user = None
        self.exclude_user = None
        for n, v in kwargs.items():
            setattr(self, n, v)


class Test_username2Uid(Suite):
    """Tests for the username2Uid function."""
    @test
    def known_user(self):
        """A known user should be found OK"""
        me = getpass.getuser()
        uid = Process.username2Uid(me)
        failIf(uid is None)
        failUnlessEqual(os.getuid(), uid)

    @test
    def unknown_user(self):
        """An unknown user should not be found OK"""
        me = getpass.getuser()
        uid = Process.username2Uid("no such user")
        failUnless(uid is None)

    @test
    def none_uers(self):
        """A username of None, returns None"""
        uid = Process.username2Uid(None)
        failUnless(uid is None)


class GetProcess(Suite):
    """Test for the getProcesses function"""
    @test
    def basicGetProcesses(self):
        """Verify basic operation."""
        this_process, processes, roots = Process.getProcesses()
        failUnlessEqual(os.getpid(), this_process.pid)
        failUnless(processes)
        failUnless(roots)
        failUnless(len(roots) < len(processes))

    @test
    def getProcesseForUser(self):
        """Get processes restricted to this user."""
        this_process, processes, roots = Process.getProcesses(
                options=Options(user=os.getuid()))
        failUnlessEqual(os.getpid(), this_process.pid)
        failUnless(processes)
        failUnless(roots)
        for p in processes:
            failUnlessEqual(os.getuid(), p.uid)

    @test
    def getProcesseNotForUser(self):
        """Get processes that are not for this user."""
        this_process, processes, roots = Process.getProcesses(
                options=Options(exclude_user=os.getuid()))
        failUnlessEqual(os.getpid(), this_process.pid)
        for p in processes:
            failIfEqual(os.getuid(), p.uid)


class Test_Process_Base(Suite):
    """Base class for process tests."""
    def find(self, cmdline):
        this_process, processes, roots = Process.getProcesses(
                options=Options(user=os.getuid()))
        return self.findInProcesses(processes, cmdline)

    def findInProcesses(self, processes, cmdline):
        for p in processes:
            if p.cmdline == cmdline:
                return p
        return None


class Test_Process(Test_Process_Base):
    """Test for the Process class.

    For these tests we use the getProcesses function to create Process
    instances and then test the details of certain process instances.

    """
    def setUp(self):
        self.sleep1_cmd_line = ("sleep", "123456")
        self.sleep2_cmd_line = ("sleep", "123457")

        # CentOS 8 seems to make use of coreutils for sleep commands changing the command line output
        self.sleep1_alt_cmd_line = (
            '/usr/bin/coreutils', '--coreutils-prog-shebang=sleep', '/usr/bin/sleep', '123456'
        )
        self.sleep2_alt_cmd_line = (
            '/usr/bin/coreutils', '--coreutils-prog-shebang=sleep', '/usr/bin/sleep', '123457'
        )

        for cmd in (self.sleep1_cmd_line, self.sleep2_cmd_line):
            failIf(self.find(cmd),
                msg="A sleep process already running\n"
                    "It may not have been killed from an earlier test.")

        self.proc1 = subprocess.Popen(self.sleep1_cmd_line)
        self.proc2 = subprocess.Popen(self.sleep2_cmd_line)
        control.delay(0.05)

    def tearDown(self):
        os.kill(self.proc1.pid, 9)
        os.kill(self.proc2.pid, 9)
        self.proc1.wait()
        self.proc2.wait()

    @test
    def findUsingRoots(self):
        """Test finding processes using find on a root process."""
        # From the root processes, we should be able to find the sleep process.
        this_process, processes, roots = Process.getProcesses(options=Options(user=os.getuid()))
        found = []
        for r in roots:
            for p in r.find(
                    lambda p: p.cmdline == self.sleep1_cmd_line or p.cmdline == self.sleep1_alt_cmd_line
            ):
                if p:
                    found.append((r, p))
        failUnless(found, msg="Could not find process as a descendant of one of the root processes")
        failUnlessEqual(1, len(found), msg="Found the process as descendant of more than one root")
        r, p = found[0]
        failIf(r is p, msg="The test process appears to be a root process")

    @test
    def clones(self):
        """Test collapsing identical commands as clones."""
        # From the root processes, we should be able to find the sleep process.
        this_process, processes, roots = Process.getProcesses(options=Options(user=os.getuid()))
        test_proc = self.findInProcesses(processes, self.sleep1_cmd_line)

        alternative = False
        if test_proc is None:
            alternative = True
            test_proc = self.findInProcesses(processes, self.sleep1_alt_cmd_line)

        # Find the parent of the test sleep processes. That should be this process.
        found = []
        for r in roots:
            for p in r.find(lambda p: test_proc in p.children):
                if p:
                    found.append((r, p))
            if found:
                break
        failIfEqual(0, len(found), msg="Failed to find expected process")
        root, parent = found[0]
        failUnlessEqual(os.getpid(), parent.pid)

        # Get clones for the parent process. We should have one clone for the
        # two sleep processes.
        clones = parent.getClones()
        failUnlessEqual(1, len(clones))
        c = clones[0]
        failUnlessEqual("sleep", c.comm)
        failUnlessEqual(2, len(c.children))
        a, b = c.children

        if alternative:
            if a.cmdline == self.sleep1_alt_cmd_line:
                failUnlessEqual(self.sleep2_alt_cmd_line, b.cmdline)
            else:
                failUnlessEqual(self.sleep2_alt_cmd_line, a.cmdline)
                failUnlessEqual(self.sleep1_alt_cmd_line, b.cmdline)
        else:
            if a.cmdline == self.sleep1_cmd_line:
                failUnlessEqual(self.sleep2_cmd_line, b.cmdline)
            else:
                failUnlessEqual(self.sleep2_cmd_line, a.cmdline)
                failUnlessEqual(self.sleep1_cmd_line, b.cmdline)


class Test_Process2(Test_Process_Base):
    """More tests for the Process class.

    For these test we have more of a process tree.
    """
    def setUp(self):
        failIf(self.find(("python", "procTree.py")),
                msg="The tree is already running"
                    "It may not have been killed from an earlier test.")

        if os.path.exists("procTree.started"):
            os.unlink("procTree.started")
        self.proc1 = subprocess.Popen(("python", "procTree.py"))
        a = time.time()
        while time.time() - a < 3:
            if os.path.exists("procTree.started"):
                break
            control.delay(0.05)
        else:
            fail("The test process tree did not start as expected")

    def tearDown(self):
        os.kill(self.proc1.pid, 15)
        if os.path.exists("procTree.started"):
            os.unlink("procTree.started")

        # Important wait for the child process of the subprocess module can
        # have problems during later cleanup.
        self.proc1.wait()

    @test
    def decendents(self):
        """Test the decendents method."""
        this_process, processes, roots = Process.getProcesses(
                options=Options(user=os.getuid()))

        # Find the parent of the test sleep processes. That should be this
        # process.
        this = None
        for r in roots:
            for p in r.find(lambda p: p.pid == os.getpid()):
                if p:
                    this = p
                    break
            if this:
                break
        failUnlessEqual(os.getpid(), this.pid)

        procs = []
        for el in this.descendants(topdown=False):
            procs.append(el)
        failUnlessEqual(3, len(procs))
        failUnlessEqual("procTree.py", procs[2].cmdline[1])
        failIfEqual("procTree.py", procs[0].cmdline[1])


if __name__ == "__main__":
    runModule()


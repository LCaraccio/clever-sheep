#!/usr/bin/env python
"""test_socket_conn.py"""
import mock
import socket
import struct
import subprocess
import time

import six

from CleverSheep.Test.Tester import *
from CleverSheep.Test.Mock.Comms import SocketConn


class ReadSocketConnTestSuite(Suite):
    """Tests for the Comms.py SocketConn where it is reading from a socket"""

    def __init__(self):
        self.recv_sock = None
        self.addr = ("", 8001)
        self.poll_manager_mock = mock.MagicMock()
        self.on_receive_mock = mock.MagicMock()
        self.on_close_mock = mock.MagicMock()
        self.on_error_mock = mock.MagicMock()

    def setUp(self):
        self.poll_manager_mock.reset()
        self.on_receive_mock.reset()
        self.on_close_mock.reset()
        self.on_error_mock.reset()

        self.recv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.recv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.recv_sock.bind(self.addr)
        self.recv_sock.listen(1)

    def tearDown(self):
        self.recv_sock.shutdown(socket.SHUT_RDWR)
        self.recv_sock.close()

    def start_message_sender(self, send_message=True, add_header=False, native=False):
        command = ["./message_sender.py"]

        if send_message:
            command.append("--send-message")

        if add_header:
            command.append("--add-header")

        if native:
            command.append("--native")

        proc = subprocess.Popen(command)

        return proc

    @test
    def test_read(self):
        """Test the read function works as expected"""
        proc = self.start_message_sender()

        s, _ = self.recv_sock.accept()

        socket_conn = SocketConn(
            s, "TestPeer", self.poll_manager_mock, onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        time.sleep(0.1)
        socket_conn._onInputActivity(None, None)

        proc.wait()
        data = socket_conn.read()

        failUnlessEqual(six.b("TestMessage"), data)
        failUnlessEqual(six.b(""), socket_conn.inBuf)

    @test
    def test_peek(self):
        """Test the peek function works as expected"""
        proc = self.start_message_sender()

        s, _ = self.recv_sock.accept()

        socket_conn = SocketConn(
            s, "TestPeer", self.poll_manager_mock, onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        time.sleep(0.1)
        socket_conn._onInputActivity(None, None)

        proc.wait()
        data = socket_conn.peek()

        failUnlessEqual(six.b("TestMessage"), data)
        failUnlessEqual(six.b("TestMessage"), socket_conn.inBuf)

    @test
    def test_read_msg_empty_buf(self):
        """Test the readMsg function when the buffer is empty"""
        proc = self.start_message_sender(send_message=False)

        s, _ = self.recv_sock.accept()

        socket_conn = SocketConn(
            s, "TestPeer", self.poll_manager_mock, onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        time.sleep(0.1)
        socket_conn._onInputActivity(None, None)

        proc.wait()
        data = socket_conn.readMsg()

        failUnlessEqual(None, data)

    @test
    def test_read_msg_success_native(self):
        """Test successfully reading a message from the buffer using native
        byte order
        """
        proc = self.start_message_sender(add_header=True, native=True)

        s, _ = self.recv_sock.accept()

        socket_conn = SocketConn(
            s, "TestPeer", self.poll_manager_mock, onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=True)

        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        time.sleep(0.1)
        socket_conn._onInputActivity(None, None)

        proc.wait()
        data = socket_conn.readMsg()

        test_msg = struct.pack("=L", 15) + "TestMessage".encode('latin-1')

        failUnlessEqual(test_msg, data)
        failUnlessEqual(six.b(""), socket_conn.inBuf)

    @test
    def test_read_msg_success_not_native(self):
        """Test successfully reading a message from the buffer when not using
        native byte order
        """
        proc = self.start_message_sender(add_header=True)

        s, _ = self.recv_sock.accept()

        socket_conn = SocketConn(
            s, "TestPeer", self.poll_manager_mock, onReceive=self.on_receive_mock, onClose=self.on_close_mock,
            onError=self.on_error_mock, native=False)

        # Trigger the callback to pull the info from the pipe, the poll manager
        # would normally call this
        time.sleep(0.1)
        socket_conn._onInputActivity(None, None)

        proc.wait()
        data = socket_conn.readMsg()

        test_msg = struct.pack("!L", 15) + "TestMessage".encode('latin-1')

        failUnlessEqual(test_msg, data)
        failUnlessEqual(six.b(""), socket_conn.inBuf)
#!/usr/bin/env python
import socket
import struct

import argparse


def send_message(send_message, add_header=False, native=False):
    addr = ("", 8001)
    send_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    send_sock.connect(addr)
    if send_message:
        test_msg = "TestMessage".encode('latin-1')
        if add_header:
            if native:
                test_msg = struct.pack("=L", len(test_msg) + 4) + test_msg
            else:
                test_msg = struct.pack("!L", len(test_msg) + 4) + test_msg
        sent = send_sock.send(test_msg)
        if not sent == len(test_msg):
            exit(1)
    exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--send-message", action="store_true")
    parser.add_argument("--add-header", action="store_true")
    parser.add_argument("--native", action="store_true")
    args = parser.parse_args()
    send_message(args.send_message, args.add_header, args.native)

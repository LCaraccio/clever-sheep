"""Hashing of classes is no longer done by default in Python3 and this class is an example of one that fails

The issue occurs as `value_that_is_instance` is a class object that has a __call__ function defined on it.
"""

from CleverSheep.Test.Tester import Suite, test
from .bad_class_to_hash import value_that_is_instance


class ClassAsImportTest(Suite):
    """Test a class as an import instance"""
    @test
    def test_class_instance_as_import(self):
        """Class instance as import"""
        value_that_is_instance()

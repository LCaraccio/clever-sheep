#!/usr/bin/env python
"""Regression test an issue that only occurred in Python3, fixed in 0.7.2

Static methods beginning with `test` would be picked up as potential tests and the code
to determine if they really are threw an exception.
"""
import CheckEnv

from CleverSheep.Test.Tester import Suite
from CleverSheep.Test.Tester import test
from CleverSheep.Test.Tester import runModule


class StaticSuite(Suite):
    """Static suite regression test"""

    @staticmethod
    def test_static_method():
        """I'm not a test"""
        pass

    @test
    def test_issue(self):
        """Placeholder test"""
        self.test_static_method()


if __name__ == "__main__":
    runModule()
